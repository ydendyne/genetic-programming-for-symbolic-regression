import numpy as np
import matplotlib.pyplot as plt
import pydotplus as pydotplus
from gplearn.genetic import SymbolicRegressor

with open(r"regression.txt") as regression:
    regression = regression.readlines()

x = []
y = []

i = 0

for line in regression:
    if i == 0:  # skip x   y
        i += 1
        continue
    arr = line.split()
    if len(arr) == 2:  # making sure it contains 2 values
        x.append([float(arr[0])])
        y.append(float(arr[1]))

regression = SymbolicRegressor(population_size=6000,
                               generations=20,
                               stopping_criteria=0.0001,
                               p_crossover=0.7,
                               p_subtree_mutation=0.1,
                               p_hoist_mutation=0.08,
                               p_point_mutation=0.1,
                               max_samples=0.9,
                               verbose=1,
                               parsimony_coefficient=0.06,
                               random_state=0,
                               metric="mean absolute error",)

regression.fit(x, y)
regression.score(x, y)

# noinspection PyProtectedMember
print(regression._program)
print("Fitness: " + str(regression.score(x, y)*100) + "%")
print("Function set: " + str(regression.function_set))

# plt.figure(1)
# plt.subplot(211)
plt.title("given values with the generated function:\ny = x^4 - 2x^3 + x^2 + 1")
plt.scatter(x, y)

xfunt = np.linspace(-2, 3, 100)
yfunt = []

for x69 in xfunt:
    y69 = (x69*x69*x69*x69) - 2*(x69*x69*x69) + (x69*x69) + 1
    yfunt.append(y69)

# plt.subplot(212)
plt.plot(xfunt, yfunt)
# plt.title("generated function")
plt.grid(True)
plt.show()

# noinspection PyProtectedMember
graph = pydotplus.graphviz.graph_from_dot_data(regression._program.export_graphviz())
graph.write_png("output.png")
